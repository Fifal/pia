let home = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;

function setActive(code, isActive) {
    let url = home + "/admin/codes/set-active";
    let data = "code=" + code + "&isActive=" + isActive;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function () {
            refresh(!isActive);
        }
    })
}

async function refresh(isActive) {
    let url = home + "/admin/codes/list-snippet";
    let data = "isActive=" + isActive;
    await sleep(100);

    $.ajax({
        type: "GET",
        url: url,
        data: data,
        success: function (data) {
            $("#list-snippet").html(data);
        }
    })
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}