let home = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;

function getTemplateData(templateId, handleData) {
    let url = home + "/user/templates/get-data";
    let data = "id=" + templateId;

    $.ajax({
        type: "GET",
        url: url,
        data: data,
        success: function (data) {
            handleData(data);
        }
    })
}

function fillFormFromData(data) {
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            $("#" + key).val(data[key]);
        }
    }
}

function accountExists(accountNumber, accountCallback, form) {
    let url = home + "/user/transaction/check-account";
    let data = "number=" + accountNumber;

    $.ajax({
        type: "GET",
        url: url,
        data: data,
        success: function (data) {
            accountCallback(data, form)
        }
    })
}

function accountCheck(data, form) {
    if (!data) {
        if (confirm("Chystáte se odeslat peníze na neexistující účet. Opravdu chcete pokračovat?")) {
            form.submit();
        }else{
            grecaptcha.reset();
        }
    } else {
        form.submit();
    }
}

function afterCaptcha(){
    let bankCode = $("#benefBankCode").val();
    let accountNumber = $("#benefAccount").val();
    let form = $("#form");

    if (bankCode === "4488") {
        accountExists(accountNumber, accountCheck, form);
    } else {
        form.submit();
    }
}

$(document).ready(function () {
    $("#templateSelect").on('change', function () {
        if (this.value === "reset") {
            $("#form").trigger("reset");
            $(":input[type=text]").val('');
        } else {
            getTemplateData(this.value, fillFormFromData);
        }
    });
});