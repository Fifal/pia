package cz.filek.pia.frontend;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Class for easier work with flash messages. Adds message to RedirectAttributes or ModelMap.
 * <p>
 * Needs JSP template file to contain <t:flash-message/> tag.
 */
public final class FlashMessages
{
    /**
     * Enum with possible types of messages
     */
    private enum FlashMessageType
    {
        ERROR("errorFlash"),
        WARNING("warningFlash"),
        INFO("infoFlash"),
        SUCCESS("successFlash");

        private String type;

        FlashMessageType(String type)
        {
            this.type = type;
        }

        public String getString()
        {
            return type;
        }
    }

    /**
     * Adds error flash message into RedirectAttributes / ModelAndView
     *
     * @param object  Object
     * @param message String message
     */
    public static void addError(Object object, String message)
    {
        FlashMessages.addMessage(object, message, FlashMessageType.ERROR);
    }

    /**
     * Adds warning flash message into RedirectAttributes / ModelAndView
     *
     * @param object  Object
     * @param message String message
     */
    public static void addWarning(Object object, String message)
    {
        FlashMessages.addMessage(object, message, FlashMessageType.WARNING);
    }

    /**
     * Adds info flash message into RedirectAttributes / ModelAndView
     *
     * @param object  Object
     * @param message String message
     */
    public static void addInfo(Object object, String message)
    {
        FlashMessages.addMessage(object, message, FlashMessageType.INFO);
    }

    /**
     * Adds success flash message into RedirectAttributes / ModelAndView
     *
     * @param object  Object
     * @param message String message
     */
    public static void addSuccess(Object object, String message)
    {
        FlashMessages.addMessage(object, message, FlashMessageType.SUCCESS);
    }

    /**
     * Adds flash message with given type into RedirectAttributes / ModelAndView
     *
     * @param object  Object
     * @param message String message
     * @param type    FlashMessageType type of message
     */
    private static void addMessage(Object object, String message, FlashMessageType type)
    {
        if (object instanceof RedirectAttributes)
        {
            ((RedirectAttributes) object).addFlashAttribute(type.getString(), message);
        }

        if (object instanceof ModelAndView)
        {
            ModelMap modelMap = ((ModelAndView) object).getModelMap();
            modelMap.addAttribute(type.getString(), message);
        }
    }

}
