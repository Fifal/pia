package cz.filek.pia.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller used for logging in and logging out
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(name = "LoginCtl")
public class LoginController extends BaseController
{
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    /**
     * Shows login form
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/login", name = "login")
    public ModelAndView login()
    {
        return new ModelAndView("customer/login/default");
    }

    /**
     * Logs out user
     */
    @GetMapping(path = "/logout", name = "logout")
    public void logout(){}
}
