package cz.filek.pia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Base Controller with transaction manager for easier transaction management
 *
 * @author Filip Jani
 */
@Controller
public abstract class BaseController
{
    @Autowired
    PlatformTransactionManager transactionManager;

    private TransactionStatus status;

    /**
     * Begins new transaction
     */
    protected void beginTransaction()
    {
        TransactionDefinition def = new DefaultTransactionDefinition();
        this.status = this.transactionManager.getTransaction(def);
    }

    /**
     * Commits transaction
     */
    protected void commitTransaction()
    {
        this.transactionManager.commit(this.status);
    }

    /**
     * Rollback for transaction
     */
    protected void rollbackTransaction()
    {
        this.transactionManager.rollback(this.status);
    }
}
