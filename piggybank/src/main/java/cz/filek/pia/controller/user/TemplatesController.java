package cz.filek.pia.controller.user;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.model.BankCode;
import cz.filek.pia.model.PaymentOrderTemplate;
import cz.filek.pia.model.UserPaymentTemplate;
import cz.filek.pia.services.BankCodeService;
import cz.filek.pia.services.PaymentTemplateService;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.*;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for managing templates for payment orders
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/user", name = "TemplatesCtl")
public class TemplatesController extends AuthController
{
    private IUserService userService;
    private IPaymentTemplateService paymentTemplateService;
    private IBankCodeService bankCodeService;

    private final String TEMPLATE_MODEL = "template";

    public TemplatesController(UserService userService,
                               PaymentTemplateService paymentTemplateService,
                               BankCodeService bankCodeService,
                               TransactionsService transactionsService)
    {
        super(transactionsService, userService);
        this.userService = userService;
        this.paymentTemplateService = paymentTemplateService;
        this.bankCodeService = bankCodeService;
    }

    /**
     * Shows list of all payment order templates
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/templates", name = "list")
    public ModelAndView listTemplates()
    {
        ModelAndView modelAndView = new ModelAndView("customer/templates/list");

        List<UserPaymentTemplate> userPaymentTemplates = this.userService.getUserPaymentTemplates(this.getUser());
        modelAndView.addObject("templatesList", userPaymentTemplates);

        return modelAndView;
    }

    /**
     * Shows edit form for existing payment order template
     *
     * @param id : ID of the template
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(path = "/templates/edit/{id}", name = "editTemplate")
    public ModelAndView editTemplate(@PathVariable int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/templates/edit", TEMPLATE_MODEL, new PaymentOrderTemplate());

        if (!this.paymentTemplateService.belongsTemplateToUser(id, this.getUser()))
        {
            FlashMessages.addError(ra, "Šablonu se nepodařilo nalézt.");
            return new ModelAndView("redirect:/user/templates");
        }


        PaymentOrderTemplate paymentTemplate = this.paymentTemplateService.getPaymentOrderTemplate(id);
        List<BankCode> bankCodeList = this.bankCodeService.getBankCodes(true);
        modelAndView.addObject("bankCodeList", bankCodeList);
        modelAndView.addObject(TEMPLATE_MODEL, paymentTemplate);

        return modelAndView;
    }

    /**
     * Processing of edit form for payment order template
     *
     * @param id : ID of the template
     * @param model : PaymentOrderTemplate
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/templates/edit/{id}", name = "editTemplateSubmit")
    public ModelAndView submitEditTemplate(@PathVariable int id,
                                           @Valid @ModelAttribute(TEMPLATE_MODEL) PaymentOrderTemplate model,
                                           BindingResult result,
                                           final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/templates/edit");

        if (!this.paymentTemplateService.belongsTemplateToUser(id, this.getUser()))
        {
            FlashMessages.addError(ra, "Šablona nebyla nalezena.");
            return new ModelAndView("redirect:/user/templates");
        }

        if (result.hasErrors())
        {
            this.addCommon(modelAndView);
            return modelAndView;
        }

        try
        {
            this.paymentTemplateService.updatePaymentOrderTemplate(model, id, this.getUserAccount());
            FlashMessages.addSuccess(ra, "Šablona byla úspěšně upravena.");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Šablonu se nepodařilo uložit.");
        }

        return new ModelAndView("redirect:/user/templates/edit/" + id);
    }

    /**
     * Shows form for adding new payment order template
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/templates/add", name = "addTemplate")
    public ModelAndView addTemplate()
    {
        ModelAndView modelAndView = new ModelAndView("customer/templates/edit", TEMPLATE_MODEL, new PaymentOrderTemplate());

        List<BankCode> bankCodeList = this.bankCodeService.getBankCodes(true);
        modelAndView.addObject("bankCodeList", bankCodeList);

        return modelAndView;
    }

    /**
     * Processing of add new template form
     *
     * @param model : PaymentOrderTemplate
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/templates/add", name = "addTemplateSubmit")
    public ModelAndView addTemplateSubmit(@Valid @ModelAttribute(TEMPLATE_MODEL) PaymentOrderTemplate model,
                                          BindingResult result,
                                          final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/templates/edit");

        if (result.hasErrors())
        {
            this.addCommon(modelAndView);
            return modelAndView;
        }

        int id = 0;

        try
        {
            PaymentOrderTemplate template = this.paymentTemplateService.addPaymentOrderTemplate(model, this.getUserAccount());
            id = template.getId();

            FlashMessages.addSuccess(ra, "Šablona byla úspěšně uložena.");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Šablonu se nepodařilo uložit.");
        }

        if(id == 0)
        {
            return new ModelAndView("redirect:/user/templates/add");
        } else
        {
            return new ModelAndView("redirect:/user/templates/edit/" + id);
        }
    }

    /**
     * Returns JSON with payment order template, so we can fill form with correct values by AJAX
     *
     * @param templateId : ID of the payment order template
     * @return JSON payload
     */
    @GetMapping(path = "/templates/get-data")
    @ResponseBody
    public PaymentOrderTemplate getTemplateData(@RequestParam("id") int templateId)
    {
        if (!this.paymentTemplateService.belongsTemplateToUser(templateId, this.getUser()))
        {
            return null;
        }

        PaymentOrderTemplate paymentOrderTemplate = this.paymentTemplateService.getPaymentOrderTemplate(templateId);
        paymentOrderTemplate.setAccount(null);

        return paymentOrderTemplate;
    }

    /**
     * Deletes user payment template from database
     *
     * @param id : ID of the template
     * @return ModelAndView
     */
    @GetMapping(path = "/templates/delete/{id}", name = "deleteTemplate")
    public ModelAndView deleteTemplate(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/user/templates");

        try
        {
            if (this.paymentTemplateService.deletePaymentOrderTemplate(id, this.getUser()))
            {
                FlashMessages.addSuccess(ra, "Šablona byla odstraněna.");
            } else
            {
                FlashMessages.addError(ra, "Šablonu se nepodařilo odstranit.");
            }
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Šablonu se nepodařilo odstranit.");
        }

        return modelAndView;
    }

    //
    //
    //
    //
    //

    /**
     * Adds common model objects into ModelAndView
     *
     * @param modelAndView ModelAndView
     */
    private void addCommon(ModelAndView modelAndView)
    {
        List<BankCode> bankCodeList = this.bankCodeService.getBankCodes(true);
        modelAndView.addObject("bankCodeList", bankCodeList);
    }
}
