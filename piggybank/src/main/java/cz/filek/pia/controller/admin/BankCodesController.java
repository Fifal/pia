package cz.filek.pia.controller.admin;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.model.BankCode;
import cz.filek.pia.services.BankCodeService;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.IBankCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * Controller for Bank Codes in Administration
 * - allows only users with 'admin' role
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/admin", name = "BankCodesCtl")
public class BankCodesController extends AuthController
{
    private final Logger logger = LoggerFactory.getLogger(BankCodesController.class);

    private IBankCodeService bankCodeService;

    /* Constants for easier edit */
    private final String BANK_MODEL = "bankCode";

    /**
     * Constructor
     *
     * @param bankCodeService : BankCodeService
     */
    public BankCodesController(BankCodeService bankCodeService,
                               TransactionsService transactionsService,
                               UserService userService)
    {
        super(transactionsService, userService);
        this.bankCodeService = bankCodeService;
    }

    /**
     * Returns view with list of bank codes based on 'is_active' status
     *
     * @param isActive boolean
     * @return ModelAndView
     */
    private ModelAndView listCodes(boolean isActive)
    {
        ModelAndView model = new ModelAndView("admin/codes/list");
        this.addCommon(model, isActive);

        return model;
    }

    /**
     * Shows page with active bank codes list
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/codes", name = "listCodes")
    public ModelAndView codesList()
    {
        return listCodes(true);
    }

    /**
     * Shows page with not active bank codes
     */
    @GetMapping(path = "/codes/not-active", name = "listNotActiveCodes")
    public ModelAndView notActiveCodesList()
    {
        return listCodes(false);
    }

    /**
     * Shows page for adding bank codes
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/codes/add", name = "addCode")
    public ModelAndView addCode()
    {
        return new ModelAndView("admin/codes/add", BANK_MODEL, new BankCode());
    }

    /**
     * Handles submit for adding new bank code
     */
    @PostMapping(path = "/codes/add/submit", name = "addCodeSubmit")
    public ModelAndView addCodeSubmit(HttpServletResponse response,
                                      @Valid @ModelAttribute(BANK_MODEL) BankCode model,
                                      BindingResult result,
                                      final RedirectAttributes ra) throws IOException
    {
        ModelAndView codeAdd = new ModelAndView("admin/codes/add");

        if (result.hasErrors())
        {
            return codeAdd;
        }

        try
        {
            this.bankCodeService.saveOrUpdate(model);
            FlashMessages.addInfo(ra, "Banka " + model.getName() + " s kódem " + model.getCode() + " byla úspěšně přidána.");
        } catch (Exception ex)
        {
            FlashMessages.addError(codeAdd, "Nepodařilo se přidat bankovní kód.");
            logger.warn("Nepodařilo se přidat bankovní kód. " + ex.getMessage());
            return codeAdd;
        }

        return new ModelAndView("redirect:/admin/codes");
    }

    /**
     * Sets code 'is_active' status based on isActive
     *
     * @param code     String code number
     * @param isActive boolean
     */
    @ResponseBody
    @RequestMapping(path = "/codes/set-active")
    public void setActive(@RequestParam("code") String code, @RequestParam("isActive") boolean isActive)
    {
        try
        {
            BankCode bankCode = this.bankCodeService.getBankCode(code);
            bankCode.setActive(isActive);
            this.bankCodeService.saveOrUpdate(bankCode);
        } catch (Exception ex)
        {
            logger.warn("Nepodařilo se nastavit status bankonvího kódu " + code + " na " + isActive + ". " + ex.getMessage());
        }
    }

    /**
     * Refresh table data
     *
     * @param isActive 'is_active' status
     * @return ModelAndView
     */
    @GetMapping(path = "/codes/list-snippet")
    public ModelAndView listSnippet(@RequestParam("isActive") boolean isActive)
    {
        ModelAndView modelAndView = new ModelAndView("admin/codes/list-snippet");
        this.addCommon(modelAndView, isActive);

        return modelAndView;
    }

    /**
     * Used for synchronization of bank codes from external source
     *
     * @param isActive : tab on which is user currently is on frontend (active, not-active codes)
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(path = "/codes/sync-codes", name = "syncCodes")
    public ModelAndView syncCodes(@RequestParam("isActive") boolean isActive, final RedirectAttributes ra)
    {
        String page = isActive ? "redirect:/admin/codes" : "redirect:/admin/codes/not-active";
        ModelAndView modelAndView = new ModelAndView(page);

        if (!this.bankCodeService.updateBankCodes())
        {
            FlashMessages.addError(ra, "Bankovní kódy se nepodařilo synchronizovat.");
        } else
        {
            FlashMessages.addSuccess(ra, "Bankovní kódy byly synchronizovány.");
        }

        return modelAndView;
    }

    /**
     * Adds common objects into ModelAndView
     *
     * @param modelAndView : ModelAndView
     * @param isActive     : boolean
     */
    private void addCommon(ModelAndView modelAndView, boolean isActive)
    {
        List<BankCode> codes = this.bankCodeService.getBankCodes(isActive);
        modelAndView.addObject("codes", codes);
        modelAndView.addObject("active", isActive);
    }
}