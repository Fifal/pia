package cz.filek.pia.controller.admin;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.ITransactionsService;
import cz.filek.pia.services.interfaces.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for admin dashboard
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/admin", name = "AdminDashboardCtl")
public class AdminDashboardController extends AuthController
{
    private ITransactionsService transactionsService;
    private IUserService userService;

    public AdminDashboardController(TransactionsService transactionsService, UserService userService)
    {
        super(transactionsService, userService);
        this.transactionsService = transactionsService;
        this.userService = userService;
    }

    /**
     * Shows default administration dashboard
     * @return ModelAndView
     */
    @GetMapping(path = "", name = "dashboard")
    public ModelAndView dashboard()
    {
        return new ModelAndView("admin/dashboard/default")
                .addObject("usersCount", this.userService.getUsersCount())
                .addObject("transactionsCount", this.transactionsService.getAccountTransactionsCount())
                .addObject("unprocessedCount", this.transactionsService.getUnprocessedPaymentsCount());
    }
}
