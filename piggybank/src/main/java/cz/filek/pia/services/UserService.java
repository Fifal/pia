package cz.filek.pia.services;

import cz.filek.pia.model.*;
import cz.filek.pia.model.enums.ERole;
import cz.filek.pia.repository.*;
import cz.filek.pia.services.interfaces.IMailService;
import cz.filek.pia.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * Service for managing users
 *
 * @author Filip Jani
 */
@Service
public class UserService implements IUserService
{
    private UserRepository userRepository;
    private AccountRepository accountRepository;
    private CardRepository cardRepository;
    private UserCardRepository userCardRepository;
    private BankCodeRepository bankCodeRepository;
    private UserAccountRepository userAccountRepository;
    private UserAddressRepository userAddressRepository;
    private UserRoleRepository userRoleRepository;
    private UserPaymentTemplatesRepository userPaymentTemplatesRepository;
    private RoleRepository roleRepository;
    private IMailService mailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${piggybank.bankcode}")
    private String bankCode;

    public UserService(UserRepository userRepository,
                       AccountRepository accountRepository,
                       CardRepository cardRepository,
                       UserCardRepository userCardRepository,
                       BankCodeRepository bankCodeRepository,
                       UserAccountRepository userAccountRepository,
                       UserAddressRepository userAddressRepository,
                       UserRoleRepository userRoleRepository,
                       UserPaymentTemplatesRepository userPaymentTemplatesRepository,
                       RoleRepository roleRepository,
                       MailService mailService,
                       BCryptPasswordEncoder bCryptPasswordEncoder)
    {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.cardRepository = cardRepository;
        this.userCardRepository = userCardRepository;
        this.bankCodeRepository = bankCodeRepository;
        this.userAccountRepository = userAccountRepository;
        this.userAddressRepository = userAddressRepository;
        this.userRoleRepository = userRoleRepository;
        this.userPaymentTemplatesRepository = userPaymentTemplatesRepository;
        this.roleRepository = roleRepository;
        this.mailService = mailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Returns List of all users sorted ASC by ID
     *
     * @return List<User>
     */
    @Override
    public List<User> getUsersList()
    {
        return userRepository.findAllByRole(ERole.user, new Sort(Sort.Direction.ASC, "id"));
    }

    /**
     * Generates new card for user with userId for account with accountId
     *
     * @param userId    : ID of user
     * @param accountId : ID of account
     */
    @Override
    public UserCard generateUserCard(int userId, int accountId)
    {
        User user = this.userRepository.findById(userId);
        Card card = generateCard(accountId);

        UserCard userCard = new UserCard();
        userCard.setCard(card);
        userCard.setUser(user);

        this.cardRepository.save(card);
        this.userCardRepository.save(userCard);

        return userCard;
    }

    /**
     * Generates new account for given user
     *
     * @param user : User
     */
    @Override
    public UserAccount generateUserAccount(User user)
    {
        BankCode bankCode = this.bankCodeRepository.findByCodeAndIsActive(this.bankCode, true);

        // Generate new account
        String accountNumber = generateAccountNumber(bankCode);
        Account account = new Account();
        account.setNumber(accountNumber);
        account.setBankCode(bankCode);

        // Assign account to user
        UserAccount userAccount = new UserAccount();
        userAccount.setAccount(account);
        userAccount.setUser(user);

        // Save to database
        this.accountRepository.save(account);
        this.userAccountRepository.save(userAccount);

        return userAccount;
    }

    /**
     * Adds new User
     *
     * @param userAddress : UserAddress model
     * @return User
     */
    @Transactional
    @Override
    public User addNewUser(UserAddress userAddress)
    {
        User user = userAddress.getUser();

        String login = this.generateLogin();
        String password = this.generatePassword();

        user.setLogin(login);
        user.setPassword(this.bCryptPasswordEncoder.encode(password));
        User savedUser = this.userRepository.save(user);

        this.userAddressRepository.save(userAddress);

        // Add user role to User
        Role role = this.roleRepository.findByRole(ERole.user);
        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);
        this.userRoleRepository.save(userRole);

        // Create new account for user
        UserAccount userAccount = this.generateUserAccount(savedUser);

        // Create card for account
        this.generateUserCard(user.getId(), userAccount.getAccount().getId());

        // Send email to client
        this.mailService.sendMailAccountCreated(user.getEmail(), login, password);

        return savedUser;
    }

    /**
     * Updates user details: User and UserAddress
     *
     * @param userAddress UserAddress
     * @return UserAddress
     */
    @Override
    public UserAddress updateUser(UserAddress userAddress)
    {
        User user = this.userRepository.findById(userAddress.getUser().getId());
        return updateUserInfo(user, userAddress);
    }

    /**
     * Updates user address info
     *
     * @param user        : User entity
     * @param userAddress : UserAddress entity
     * @return UserAddress
     */
    @Override
    public UserAddress updateUserAddress(User user, UserAddress userAddress)
    {
        UserAddress ua = this.getUserAddress(user.getId());

        if (userAddress.getId() != ua.getId())
        {
            userAddress.setId(ua.getId());
        }

        return updateUserInfo(user, userAddress);
    }

    /**
     * Helper function to fill some important values in entity when editing
     *
     * @param user        : User entity
     * @param userAddress : UserAddress entity
     * @return UserAddress
     */
    private UserAddress updateUserInfo(User user, UserAddress userAddress)
    {
        User userForm = userAddress.getUser();
        userForm.setId(user.getId());
        userForm.setLogin(user.getLogin());
        userForm.setPassword(user.getPassword());

        this.userRepository.save(userForm);
        this.userAddressRepository.save(userAddress);

        return userAddress;
    }

    /**
     * Deletes user from database
     *
     * @param userId : ID of the user
     * @return true if success, false otherwise
     */
    @Transactional
    @Override
    public boolean deleteUser(int userId)
    {
        User user = this.userRepository.findById(userId);
        if (user == null)
        {
            return false;
        }

        UserAccount userAccount = this.getUserAccount(user);

        this.userRepository.delete(user);

        // Send email to client
        this.mailService.sendMailAccountDeleted(user.getEmail(), userAccount);

        return true;
    }

    /**
     * Returns user by his ID
     *
     * @param userId : ID of the user
     * @return User
     */
    @Override
    public User getUser(int userId)
    {
        return this.userRepository.findById(userId);
    }

    /**
     * Returns user address by user ID
     *
     * @param userId : ID of the user
     * @return UserAddress
     */
    @Override
    public UserAddress getUserAddress(int userId)
    {
        return this.userAddressRepository.findUserAddressByUserId(userId);
    }

    /**
     * Returns list with user's accounts
     *
     * @param userId : ID of the user
     * @return List<UserAccount>
     */
    @Override
    public List<UserAccount> getUserAccounts(int userId)
    {
        return this.userAccountRepository.findUserAccountsByUserId(userId);
    }

    /**
     * Returns users account
     *
     * @param user : User entity
     * @return UserAccount
     */
    private UserAccount getUserAccount(User user)
    {
        List<UserAccount> userAccounts = this.userAccountRepository.findUserAccountsByUserId(user.getId());
        if (userAccounts != null && userAccounts.size() != 0)
        {
            return userAccounts.get(0);
        }

        return null;
    }

    /**
     * Returns list with user's cards
     *
     * @param userId : ID of the user
     * @return List<UserCard>
     */
    @Override
    public List<UserCard> getUserCards(int userId)
    {
        return this.userCardRepository.findUserCardsByUserId(userId);
    }

    /**
     * Deletes card by id
     *
     * @param cardId : ID of the card
     */
    @Override
    public boolean deleteCard(int cardId)
    {
        UserCard userCard = this.userCardRepository.findUserCardByCardId(cardId);
        if (userCard == null)
        {
            return false;
        }

        this.cardRepository.delete(userCard.getCard());

        return true;
    }

    /**
     * Returns UserAccount entity by account ID
     *
     * @param accountId : ID of the account
     * @return UserAccount
     */
    @Override
    public UserAccount getUserAccount(int accountId)
    {
        return this.userAccountRepository.findUserAccountByAccountId(accountId);
    }

    /**
     * Returns User by card ID
     *
     * @param cardId : ID of the card
     * @return User
     */
    @Override
    public User getUserByCardId(int cardId)
    {
        UserCard userCard = this.userCardRepository.findUserCardByCardId(cardId);

        if (userCard == null)
        {
            return null;
        }

        return userCard.getUser();
    }

    /**
     * Returns User by login
     *
     * @param login : Login of the user
     * @return User
     */
    @Override
    public User getUserByLogin(String login)
    {
        return this.userRepository.findUserByLogin(login);
    }

    /**
     * Returns User's role list
     *
     * @param user : User entity
     * @return List<UserRole>
     */
    @Override
    public List<UserRole> getUserRole(User user)
    {
        return this.userRoleRepository.findUserRolesByUserId(user.getId());
    }

    /**
     * Generates random PIN for user to login with
     * - Numeric from range: 1000 - 9999
     *
     * @return String
     */
    private String generatePassword()
    {
        return randomNumericWithDigits(4);
    }

    /**
     * Generates random user's login number
     *
     * @return String
     */
    private String generateLogin()
    {
        String login;
        do
        {
            login = randomNumericWithDigits(8);
        }
        while (this.userRepository.findUserByLogin(login) != null);

        return login;
    }

    /**
     * Generates random account number
     *
     * @return String
     */
    private String generateAccountNumber(BankCode bankCode)
    {
        String account;
        do
        {
            account = randomNumericWithDigits(10);

        } while (this.accountRepository.findByNumberAndBankCode(account, bankCode) != null);

        return account;
    }

    /**
     * Returns list of all user's templates
     *
     * @param user : User entity
     * @return List<UserPaymentTemplate>
     */
    @Override
    public List<UserPaymentTemplate> getUserPaymentTemplates(User user)
    {
        return this.userPaymentTemplatesRepository.findByUser(user);
    }

    /**
     * Returns count of clients - users with role user
     *
     * @return Integer
     */
    @Override
    public Integer getUsersCount()
    {
        return this.userRepository.countAllByRole(ERole.user);
    }

    //
    //
    // Utilities
    //
    //

    /**
     * Generates new card with random number and CVV
     *
     * @param accountId : ID of account
     * @return Card
     */
    private Card generateCard(int accountId)
    {
        Account account = this.accountRepository.findById(accountId);

        Card card = new Card();
        card.setAccount(account);
        card.setBlocked(false);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2);
        Date from = new Date(System.currentTimeMillis());
        Date to = new Date(calendar.getTimeInMillis());

        card.setValidFrom(from);
        card.setValidTo(to);

        String cardNumber;
        do
        {
            cardNumber = randomNumericWithDigits(16);
        } while (this.cardRepository.findByNumber(cardNumber) != null);

        card.setNumber(cardNumber);
        card.setCvv(randomNumericWithDigits(3));

        return card;
    }

    /**
     * Generates numeric string with maximum N digits
     *
     * @param digCount : int digit count
     * @return String
     */
    private String randomNumericWithDigits(int digCount)
    {
        Random random = new Random();

        StringBuilder sb = new StringBuilder(digCount);
        for (int i = 0; i < digCount; i++)
            sb.append((char) ('0' + random.nextInt(10)));
        return sb.toString();
    }
}
