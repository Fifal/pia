package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.PaymentOrderTemplate;
import cz.filek.pia.model.User;
import cz.filek.pia.model.UserAccount;

/**
 * Interface for payment template service
 *
 * @author Filip Jani
 */
public interface IPaymentTemplateService
{
    /**
     * Returns PaymentOrderTemplate by its ID
     *
     * @param id : ID of the paymentOrderTemplate
     * @return PaymentOrderTemplate
     */
    PaymentOrderTemplate getPaymentOrderTemplate(int id);

    /**
     * Updates PaymentOrderTemplate
     *
     * @param paymentOrderTemplate : PaymentOrderTemplate
     * @param id : ID of the template when editing
     * @param userAccount : UserAccount of the user
     * @return true if successful, false otherwise
     */
    boolean updatePaymentOrderTemplate(PaymentOrderTemplate paymentOrderTemplate, int id, UserAccount userAccount);

    /**
     * Adds new PaymentOrderTemplate and assigns it to user by his User Account entity
     *
     * @param paymentOrderTemplate : Model with template
     * @param userAccount : UserAccount
     * @return created PaymentOrderTemplate
     */
    PaymentOrderTemplate addPaymentOrderTemplate(PaymentOrderTemplate paymentOrderTemplate, UserAccount userAccount);

    /**
     * Checks if template belongs to User
     *
     * @param templateId : ID of the PaymentOrderTemplate
     * @return true if does, false otherwise
     */
    boolean belongsTemplateToUser(int templateId, User user);

    /**
     * Deletes PaymentOrderTemplate from database
     *
     * @param templateId : ID of the template
     * @return true if successful, false otherwise
     */
    boolean deletePaymentOrderTemplate(int templateId, User user);
}
