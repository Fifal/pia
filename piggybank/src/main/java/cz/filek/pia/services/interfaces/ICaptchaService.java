package cz.filek.pia.services.interfaces;

/**
 * Interface for captcha service
 */
public interface ICaptchaService
{
    /**
     * Processes response and returns if captcha was validated successfully
     * @param response : String response
     * @return true if captcha was validated, false otherwise
     */
    boolean processResponse(String response);
}
