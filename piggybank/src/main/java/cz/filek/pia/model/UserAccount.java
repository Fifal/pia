package cz.filek.pia.model;

import javax.persistence.*;

/**
 * Entity for user accounts
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user_accounts")
public class UserAccount
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "account_id", unique = true)
    private Account account;

    /**
     * Returns PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns user entity
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Setter for user entity
     * @param user User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Returns account entity
     * @return Account
     */
    public Account getAccount()
    {
        return account;
    }

    /**
     * Setter for account entity
     * @param account Account
     */
    public void setAccount(Account account)
    {
        this.account = account;
    }
}
