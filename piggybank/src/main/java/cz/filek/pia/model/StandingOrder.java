package cz.filek.pia.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

/**
 * Entity for standing order
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "standing_order")
public class StandingOrder
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Integer id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    protected Account account;

    @Column(name = "message", length = 255)
    @Size(max = 255, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String message;

    @Column(name = "const_symbol", length = 10)
    @Size(max = 10, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String constSymbol;

    @Column(name = "var_symbol", length = 10)
    @Size(max = 10, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String varSymbol;

    @Column(name = "spec_symbol", length = 10)
    @Size(max = 10, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String specSymbol;

    @Column(name = "benef_account", length = 15)
    @Size(min = 10, max = 15, message = "Číslo účtu musí být rozmezí {min} - {max} znaků.")
    protected String benefAccount;

    @Column(name = "benef_bank_code", length = 4)
    @Size(min = 4, max = 4, message = "Kód banky Musí mít {max} znaky.")
    protected String benefBankCode;

    @Column(name = "amount")
    @Digits(integer = 9, fraction = 0, message = "Částka smí obsahovat pouze číslice.")
    protected Integer amount;

    @NotNull(message = "Počátek odesílání musí být vyplněn.")
    @Column(name = "standing_start_date")
    protected Date standingStartDate;

    @NotNull(message = "Konec odesílání musí být vyplněn.")
    @Column(name = "standing_stop_date")
    protected Date standingStopDate;

    @NotNull(message = "Doba opakování musí být nastavena.")
    @Column(name = "period")
    protected Integer period;

    @Column(name = "title", length = 255)
    @Size(max = 255, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String title;

    @Column(name = "active")
    private boolean isActive;

    /**
     * Returns PK ID
     * @return int
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Setter or ID
     * @param id int
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * Returns account entity
     * @return Account
     */
    public Account getAccount()
    {
        return account;
    }

    /**
     * Setter for account entity
     * @param account Account
     */
    public void setAccount(Account account)
    {
        this.account = account;
    }

    /**
     * Returns message for receiver
     * @return String
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Setter for message for receiver
     * @param message String
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * Returns constant symbol
     * @return String
     */
    public String getConstSymbol()
    {
        return constSymbol;
    }

    /**
     * Setter for constant symbol
     * @param constSymbol String
     */
    public void setConstSymbol(String constSymbol)
    {
        this.constSymbol = constSymbol;
    }

    /**
     * Returns variable symbol
     * @return String
     */
    public String getVarSymbol()
    {
        return varSymbol;
    }

    /**
     * Seter for variable symbol
     * @param varSymbol String
     */
    public void setVarSymbol(String varSymbol)
    {
        this.varSymbol = varSymbol;
    }

    /**
     * Returns specific symbol
     * @return String
     */
    public String getSpecSymbol()
    {
        return specSymbol;
    }

    /**
     * Setter for specific symbol
     * @param specSymbol String
     */
    public void setSpecSymbol(String specSymbol)
    {
        this.specSymbol = specSymbol;
    }

    /**
     * Returns recipient's account number, including prefix, in format: ####-##########
     * @return String
     */
    public String getBenefAccount()
    {
        return benefAccount;
    }

    /**
     * Setter for recipient's account number
     * @param benefAccount String
     */
    public void setBenefAccount(String benefAccount)
    {
        this.benefAccount = benefAccount;
    }

    /**
     * Returns recipient's bank code
     * @return String
     */
    public String getBenefBankCode()
    {
        return benefBankCode;
    }

    /**
     * Setter for recipient's bank code
     * @param benefBankCode String
     */
    public void setBenefBankCode(String benefBankCode)
    {
        this.benefBankCode = benefBankCode;
    }

    /**
     * Returns amount of money
     * @return int
     */
    public Integer getAmount()
    {
        return amount;
    }

    /**
     * Setter for amount
     * @param amount int
     */
    public void setAmount(Integer amount)
    {
        this.amount = amount;
    }

    /**
     * Returns when standing order will start
     * @return Date
     */
    public Date getStandingStartDate()
    {
        return standingStartDate;
    }

    /**
     * Setter for standing date start
     * @param standingStartDate Date
     */
    public void setStandingStartDate(Date standingStartDate)
    {
        this.standingStartDate = standingStartDate;
    }

    /**
     * Returns when standing order will stop
     * @return Date
     */
    public Date getStandingStopDate()
    {
        return standingStopDate;
    }

    /**
     * Setter for standing date stop
     * @param standingStopDate Date
     */
    public void setStandingStopDate(Date standingStopDate)
    {
        this.standingStopDate = standingStopDate;
    }

    /**
     * Returns title of the template
     * @return String
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Setter for the template title
     * @param title String
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Returns how often is order processed
     * @return Integer
     */
    public Integer getPeriod()
    {
        return period;
    }

    /**
     * Setter for period
     * @param period Integer
     */
    public void setPeriod(Integer period)
    {
        this.period = period;
    }

    /**
     * Returns if standing order is active
     * @return boolean
     */
    public boolean isActive()
    {
        return isActive;
    }

    /**
     * Setter for active
     * @param active boolean
     */
    public void setActive(boolean active)
    {
        isActive = active;
    }

    /**
     * Returns full account number:
     *      - Account number/Bank Code
     *
     * @return String
     */
    public String getFullAccountNumber()
    {
        return this.benefAccount + "/" + this.benefBankCode;
    }
}
