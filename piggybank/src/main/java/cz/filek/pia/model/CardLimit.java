package cz.filek.pia.model;

import cz.filek.pia.model.enums.ELimit;
import cz.filek.pia.model.enums.PostgresSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * Entity for Card limit
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "card_limits")
@TypeDef(name = "eLimit", typeClass = PostgresSQLEnumType.class)
public class CardLimit
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "card_id")
    private Card card;

    @Enumerated(EnumType.STRING)
    @Column(name = "limit")
    @Type(type = "eLimit")
    private ELimit limit;

    /**
     * Returns limit id
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns Card entity
     * @return Card
     */
    public Card getCard()
    {
        return card;
    }

    /**
     * Setter for Card entity
     * @param card Card
     */
    public void setCard(Card card)
    {
        this.card = card;
    }

    /**
     * Returns type of limit
     * @return ELimit
     */
    public ELimit getLimit()
    {
        return limit;
    }

    /**
     * Setter for type of limit
     * @param limit ELimit
     */
    public void setLimit(ELimit limit)
    {
        this.limit = limit;
    }
}
