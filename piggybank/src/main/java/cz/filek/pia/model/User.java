package cz.filek.pia.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Entity for user
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user", schema = "public")
public class User implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "login", nullable = false, length = 32, unique = true)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false, length = 100, unique = true)
    @NotNull
    @Email(message = "Emailová adresa musí být ve správném formátu.")
    @Size(min = 5, max = 100, message = "Email musí být v rozmezí {min} - {max} znaků.")
    private String email;

    @Column(name = "firstname", nullable = false, length = 30)
    @NotNull
    @Size(min = 2, max = 30, message = "Jméno musí být v rozmezí {min} - {max} znaků.")
    private String firstName;

    @Column(name = "lastname", nullable = false, length = 60)
    @NotNull
    @Size(min = 1, max = 60, message = "Příjmení musí být v rozmezí {min} - {max} znaků.")
    private String lastName;

    @Column(name = "phone_number", nullable = true, length = 9)
    @Size(max = 9, message = "Délka čísla musí být maximálně {max} znaků.")
    private String phoneNumber;

    @Column(name = "pin", nullable = false, length = 11, unique = true)
    @NotNull
    @Size(min = 11, max = 11, message = "Rodné číslo musí mít {max} znaků.")
    private String pin;

    public User(){}

    /**
     * Constructor for user entity
     * @param login login
     * @param password password
     * @param email email
     * @param firstName first name
     * @param lastName last name
     * @param phoneNumber phone number
     */
    public User(String login, String password, String email, String firstName, String lastName, String phoneNumber)
    {
        this.login = login;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    /**
     * Returns PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for user's id
     * @param id int
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Returns user's login
     * @return String
     */
    public String getLogin()
    {
        return login;
    }

    /**
     * Setter for user's login
     * @param login String
     */
    public void setLogin(String login)
    {
        this.login = login;
    }

    /**
     * Returns user's password
     * @return String
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Setter for user's password
     * @param password String
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Returns user's email
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Setter for user's email
     * @param email String
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * Returns user's first name
     * @return String
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * Setter for user's first name
     * @param firstName String
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * Returns user's last name
     * @return String
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     *  Setter for user's last name
     * @param lastName String
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * Returns user's phone number
     * @return String
     */
    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    /**
     * Setter for user's phone number
     * @param phoneNumber String
     */
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Returns user's PIN
     * @return String
     */
    public String getPin()
    {
        return pin;
    }

    /**
     * Setter for user's Personal Identification Number
     * @param pin String
     */
    public void setPin(String pin)
    {
        this.pin = pin;
    }

    /**
     * Returns user's full name: firstname lastname
     * @return String
     */
    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }
}
