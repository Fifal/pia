package cz.filek.pia.model.enums;

import java.io.Serializable;

/**
 * Enum for types of transactions
 *
 * @author Filip Jani
 */
public enum ETransaction implements Serializable
{
    card,
    atm,
    bank;

    public String getText(ETransaction type)
    {
        switch (type)
        {
            case atm:
            {
                return "Výběr z bankomatu";
            }
            case bank:
            {
                return "Bankovní převod";
            }
            case card:
            {
                return "Platba kartou";
            }
            default:
            {
                return "Bankovní převod";
            }
        }
    }
}
