package cz.filek.pia.model;

import javax.persistence.*;

@Entity
@Table(name = "user_standing_orders")
public class UserStandingOrder
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "standing_order_id")
    private StandingOrder standingOrder;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public StandingOrder getStandingOrder()
    {
        return standingOrder;
    }

    public void setStandingOrder(StandingOrder standingOrder)
    {
        this.standingOrder = standingOrder;
    }

}
