package cz.filek.pia.model;

import javax.persistence.*;

/**
 * Entity for user payment order templates
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user_payment_templates")
public class UserPaymentTemplate
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "payment_order_template_id")
    private PaymentOrderTemplate paymentOrderTemplate;

    /**
     * Returns id
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for id
     * @param id int
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Returns User entity
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Setter for User entity
     * @param user User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Returns PaymentOrderTemplate entity
     * @return PaymentOrderTemplate
     */
    public PaymentOrderTemplate getPaymentOrderTemplate()
    {
        return paymentOrderTemplate;
    }

    /**
     * Setter for PaymentOrderTemplate entity
     * @param paymentOrderTemplate PaymentOrderTemplate
     */
    public void setPaymentOrderTemplate(PaymentOrderTemplate paymentOrderTemplate)
    {
        this.paymentOrderTemplate = paymentOrderTemplate;
    }
}
