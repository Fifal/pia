package cz.filek.pia.model;

import javax.persistence.*;

/**
 * Entity for user cards
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user_cards")
public class UserCard
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "card_id", unique = true)
    private Card card;

    /**
     * Return PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns user entity
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Setter for user entity
     * @param user User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Returns card entity
     * @return Card
     */
    public Card getCard()
    {
        return card;
    }

    /**
     * Setter for card entity
     * @param card Card
     */
    public void setCard(Card card)
    {
        this.card = card;
    }
}
