package cz.filek.pia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PiggybankApplication extends SpringBootServletInitializer
{

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
        return builder.sources(PiggybankApplication.class);
    }

    public static void main (String[] args) {

        SpringApplication app =
                  new SpringApplication(PiggybankApplication.class);
        app.run(args);
    }
}