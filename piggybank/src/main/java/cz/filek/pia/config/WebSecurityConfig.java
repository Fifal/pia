package cz.filek.pia.config;

import cz.filek.pia.model.enums.ERole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
    /**
     * BCrypt encryption for passwords
     */
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Data source
     */
    private DataSource dataSource;

    /**
     * SQL query used for getting user by login
     */
    @Value("${spring.queries.users-query}")
    private String usersQuery;

    /**
     * SQL query used for selecting user roles
     */
    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
                             DataSource dataSource)
    {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.dataSource = dataSource;
    }

    /**
     * Configuration of authentication manager
     *
     * @param authenticationMgr AuthenticationManagerBuilder
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception
    {
        authenticationMgr
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    /**
     * Configuration of access for users to given url paths by their role
     *
     * @param http HttpSecurity
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
                .antMatchers("/", "/login", "/error").permitAll()
                .antMatchers("/user/**").hasAuthority(ERole.user.toString())
                .antMatchers("/admin/**").hasAuthority(ERole.admin.toString())
                .anyRequest().authenticated()
                .and().csrf().disable().formLogin()
                .loginPage("/login").failureUrl("/login?error=true")
                .defaultSuccessUrl("/")
                .usernameParameter("login")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login").and().exceptionHandling();
    }

    /**
     * Allow access to folders with resources
     *
     * @param web WebSecurity
     */
    @Override
    public void configure(WebSecurity web)
    {
        web.ignoring()
                .antMatchers("/resources/**", "/static/**");
    }

}
