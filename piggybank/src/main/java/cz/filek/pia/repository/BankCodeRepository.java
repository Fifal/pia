package cz.filek.pia.repository;

import cz.filek.pia.model.BankCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for bank_codes
 *
 * @author Filip Jani
 */
@Repository
public interface BankCodeRepository extends JpaRepository<BankCode, Long>
{
    /**
     * Find Bank Code entity by 'code'
     *
     * @param code String
     * @return BankCode
     */
    BankCode findByCodeAndIsActive(String code, boolean isActive);

    /**
     * Find Bank Code entity by 'code'
     *
     * @param code String
     * @return BanKCode
     */
    BankCode findByCode(String code);

    /**
     * Find list of Bank Code entities by 'is_active'
     *
     * @param isActive boolean
     * @return List<BankCode>
     */
    List<BankCode> findByIsActiveOrderByCodeAsc(boolean isActive);

    /**
     * Finds active Bank Code by name
     *
     * @param name String
     * @return BankCode
     */
    @Query("SELECT code FROM BankCode code WHERE name=?1 AND is_active=true")
    BankCode findActiveByName(String name);
}
