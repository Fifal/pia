package cz.filek.pia.repository;

import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.StandingOrderLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository for standing_order_log
 *
 * @author Filip Jani
 */
@Repository
public interface StandingOrderLogRepository extends JpaRepository<StandingOrderLog, Integer>
{
    /**
     * Finds StandingOrderLog by StandingOrder
     *
     * @param standingOrder : StandingOrder
     * @return StandingOrderLog
     */
    @Query("SELECT sol FROM StandingOrderLog sol JOIN StandingOrder so ON sol.standingOrder.id = so.id WHERE so = ?1")
    List<StandingOrderLog> findAllByStandingOrder(StandingOrder standingOrder);

    /**
     * Finds last time standing order was processed
     *
     * @param standingOrder : StandingOrder
     * @return LocalDateTime
     */
    @Query("SELECT max(sol.sentDateTime) FROM StandingOrderLog sol JOIN StandingOrder so ON sol.standingOrder.id = so.id WHERE so = ?1")
    LocalDateTime findLastDateTimeSent(StandingOrder standingOrder);

    /**
     * Returns StandingOrderLog by date and standing order ID
     *
     * @param date : LocalDateTime
     * @param id : ID of the StandingOrder
     * @return StandingOrderLog
     */
    @Query("SELECT sol FROM StandingOrderLog sol JOIN StandingOrder so ON sol.standingOrder.id = so.id WHERE sol.sentDateTime =?1 AND so.id = ?2")
    StandingOrderLog findBySentDateTimeAndId(LocalDateTime date, Integer id);
}
