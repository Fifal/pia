package cz.filek.pia.repository;

import cz.filek.pia.model.CardLimit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for card_limits
 */
@Repository
public interface CardLimitRepository extends JpaRepository<CardLimit, Long>
{
    /**
     * Finds list of Card Limits entities by 'card_id' column
     * @param id card id
     * @return List<CardLimit>
     */
    List<CardLimit> findCardLimitsByCardId(int id);
}
