package cz.filek.pia.repository;

import cz.filek.pia.model.PaymentOrderTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for payment_order_template
 *
 * @author Filip Jani
 */
@Repository
public interface PaymentOrderTemplateRepository extends JpaRepository<PaymentOrderTemplate, Long>
{
    /**
     * Returns Payment order template by ID
     * @param id : ID of the template
     * @return PaymentOrderTemplate
     */
    PaymentOrderTemplate findById(int id);
}
