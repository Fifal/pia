package cz.filek.pia.repository;

import cz.filek.pia.model.Role;
import cz.filek.pia.model.enums.ERole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for role
 *
 * @author Filip Jani
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>
{

    /**
     * Returns Role entity by role type
     * @param role : ERole
     * @return Role
     */
    Role findByRole(ERole role);
}
