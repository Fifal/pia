package cz.filek.pia.repository;

import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.User;
import cz.filek.pia.model.UserStandingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user_standing_orders table
 *
 * @author Filip Jani
 */
@Repository
public interface UserStandingOrderRepository extends JpaRepository<UserStandingOrder, Integer>
{
    /**
     * Finds UserStandingOrder by StandingOrder entity
     *
     * @param standingOrder : StandingOrder
     * @return UserStandingOrder
     */
    @Query("SELECT uso FROM UserStandingOrder uso JOIN StandingOrder so ON uso.standingOrder.id = so.id WHERE so = ?1")
    UserStandingOrder findByStandingOrder(StandingOrder standingOrder);

    /**
     * Finds UserStandingOrder by StandingOrder ID
     *
     * @param id : ID of the standing order
     * @return UserStandingOrder
     */
    @Query("SELECT uso FROM UserStandingOrder uso JOIN StandingOrder so ON uso.standingOrder.id = so.id WHERE so.id = ?1")
    UserStandingOrder findByStandingOrderId(int id);

    /**
     * Finds all user standing orders
     *
     * @param user : User enitty
     * @return List<UserStandingOrder>
     */
    List<UserStandingOrder> findAllByUser(User user);
}
