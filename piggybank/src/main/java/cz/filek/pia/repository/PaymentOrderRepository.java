package cz.filek.pia.repository;

import cz.filek.pia.model.PaymentOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repostiory for payment_order
 *
 * @author Filip Jani
 */
@Repository
public interface PaymentOrderRepository extends JpaRepository<PaymentOrder, Long>
{
    /**
     * Returns all payment orders with processed date equal to NULL
     * @return List<PaymentOrder>
     */
    List<PaymentOrder> findAllByProcessedDateIsNull();

    /**
     * Returns all payment orders for given account which hasn't been processed yet
     * @return List<PaymentOrder>
     */
    List<PaymentOrder> findAllByAccountIdAndProcessedDateIsNull(int accountId);

    /**
     * Returns count of all unprocessed payments
     * @return Integer
     */
    Integer countAllByProcessedDateIsNull();
}
