package cz.filek.pia.repository;

import cz.filek.pia.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for card
 *
 * @author Filip Jani
 */
public interface CardRepository extends JpaRepository<Card, Long>
{
    /**
     * Finds Card entity by ID
     *
     * @param id int
     * @return Card entity
     */
    Card findById(int id);

    /**
     * Finds Card entity by number
     *
     * @param number int
     * @return Card entity
     */
    Card findByNumber(String number);
}
