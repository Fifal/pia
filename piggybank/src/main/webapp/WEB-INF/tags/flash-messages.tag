<%@ tag description="Template for flash messages" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="errorFlash"%>
<%@attribute name="warningFlash"%>
<%@attribute name="infoFlash"%>
<%@attribute name="successFlash"%>

<c:if test="${errorFlash != null}">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible fade show text-small" role="alert">
                <strong>Chyba!</strong> ${errorFlash}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${warningFlash != null}">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible fade show text-small" role="alert">
                <strong>Varování!</strong> ${warningFlash}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${infoFlash != null}">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info alert-dismissible fade show text-small" role="alert">
                <strong>Info!</strong> ${infoFlash}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${successFlash != null}">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible fade show text-small" role="alert">
                <strong>Úspěch!</strong> ${successFlash}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-12">
        <div id="ajax-alert"></div>
    </div>
</div>