<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="scripts" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap-reboot.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/style.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="${request.contextPath}/resources/customer/js/bootstrap.bundle.min.js"></script>

    <title>Piggy Bank |
        <jsp:invoke fragment="title"/>
    </title>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-green">
    <div class="container">
        <a class="navbar-brand" href="${s:mvcUrl('DashboardCtl#dashboard').build()}"><span class="fa fa-piggy-bank"></span> Piggy Bank</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <%-- URI links definition --%>
        <c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="page"/>
        <c:set var="dashboardUri" value="${s:mvcUrl('DashboardCtl#dashboard').build()}" scope="page"/>
        <c:set var="templatesUri" value="${s:mvcUrl('TemplatesCtl#list').build()}" scope="page"/>
        <c:set var="standingUri" value="${s:mvcUrl('StandingOrderCtl#list').build()}" scope="page"/>
        <c:set var="historyUri" value="${s:mvcUrl('TransactionCtl#history').build()}" scope="page"/>
        <%----%>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ${fn:contains(currentUri, dashboardUri) ? ' active' : ''}">
                    <a class="nav-link" href="${dashboardUri}">Přehled</a>
                </li>
                <li class="nav-item ${fn:contains(currentUri, templatesUri) ? ' active' : ''}">
                    <a class="nav-link" href="${templatesUri}">Šablony plateb</a>
                </li>
                <li class="nav-item ${fn:contains(currentUri, standingUri) ? ' active' : ''}">
                    <a class="nav-link" href="${standingUri}">Trvalé příkazy</a>
                </li>
                <li class="nav-item ${fn:contains(currentUri, historyUri) ? ' active' : ''}">
                    <a class="nav-link" href="${historyUri}">Historie transakcí</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${userEntity.getFullName()}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="${s:mvcUrl("SettingsCtl#settings").build()}">Nastavení</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="${s:mvcUrl("LoginCtl#logout").build()}">Odhlásit se</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container container-main">
    <jsp:doBody/>
</div>

<jsp:invoke fragment="scripts"/>
</body>
</html>