<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin/users" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<t:layout-admin>
    <jsp:attribute name="title">Bankovní kódy</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <tc:side-menu/>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Přehled zákazníků</div>
                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <tr>
                                <th class="col-1" scope="col">#</th>
                                <th class="col-4" scope="col">Jméno</th>
                                <th class="col-6" scope="col">Příjmení</th>
                                <th class="text-right col-1" scope="col">Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${users}" var="user">
                                <tr class="text-small">
                                    <th scope="row">${user.getId()}</th>
                                    <td>${user.getFirstName()}</td>
                                    <td>${user.getLastName()}</td>
                                    <td class="text-right">
                                        <a href="${s:mvcUrl("UsersCtl#editUser").arg(0, user.id).build()}"
                                           title="Editovat"><span class="fa fa-pencil-alt fa-fw"></span></a>
                                        <a href="${s:mvcUrl("UsersCtl#deleteUser").arg(0, user.id).build()}"
                                           title="Smazat"><span class="fa fa-trash fa-fw a-trash"></span></a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-admin>