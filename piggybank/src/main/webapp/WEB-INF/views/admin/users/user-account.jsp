<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="col-md-9 offset-md-3">
    <div class="card">
        <div class="card-header">Manuální přidávání pěněz na účet</div>
        <div class="card-body text-small">
            <form:form action="${s:mvcUrl('UsersCtl#topUpAccount').build()}" method="post" modelAttribute="account">

                <%-- TODO: chybový hlášky --%>
                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <label for="id">Účet</label>
                    </div>
                    <div class="col-md-5">
                        <form:select path="id" cssClass="form-control">
                            <form:options items="${userAccounts}" itemValue="account.id" itemLabel="account.string"/>
                        </form:select>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-2 offset-md-1">
                        <label for="amount">Částka</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" id="amount" name="amount" class="form-control" placeholder="150"/>
                    </div>
                </div>

                <input type="hidden" name="userId" id="userId" value="${user.user.id}"/>

                <div class="row">
                    <div class="col-md-5 offset-md-3">
                        <button class="btn btn-outline-primary btn-sm form-control" type="submit"
                                title="Přidá peníze na účet">
                            Přidat peníze na účet
                        </button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>