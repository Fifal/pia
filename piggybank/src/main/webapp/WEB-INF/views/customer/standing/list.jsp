<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Trvalé příkazy</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Přehled trvalých příkazů</div>
                    <div class="card-body">

                        <!-- Nadpis, popis, rozpis -->
                        <div class="row mb-5">
                            <div class="col-12">
                                <p class="text-small">
                                    Na následující stránce můžete spravovat své trvalé příkazy.
                                    <a href="${s:mvcUrl('StandingOrderCtl#addOrder').build()}"
                                       class="btn btn-sm btn-outline-primary float-right"><span
                                            class="fa fa-plus"></span> Přidat nový trvalý příkaz</a>
                                </p>
                            </div>
                        </div>

                        <hr>

                        <!-- Titulky tabulky -->
                        <div class="row mb-3">
                            <div class="col-4"><span class="text-small text-bold">Název</span></div>
                            <div class="col-4 text-center"><span class="text-small text-bold">Popis</span></div>
                            <div class="col-4 text-right"><span class="text-small text-bold">Možnosti</span></div>
                        </div>

                        <hr>

                        <c:forEach var="order" items="${userStandingOrders}">
                            <div class="row mb-3">

                                <div class="col-4 mb-auto mt-auto">${order.standingOrder.title}</div>

                                <div class="col-4">
                                    <div class="text-small"><span
                                            class="font-italic">účet: </span><span>${order.standingOrder.fullAccountNumber}</span>
                                    </div>
                                    <c:if test="${order.standingOrder.varSymbol != null && !order.standingOrder.varSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">VS: </span><span>${order.standingOrder.varSymbol}</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${order.standingOrder.constSymbol != null && !order.standingOrder.constSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">KS: </span><span>${order.standingOrder.constSymbol}</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${order.standingOrder.specSymbol != null && !order.standingOrder.specSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">SS: </span><span>${order.standingOrder.specSymbol}</span>
                                        </div>
                                    </c:if>

                                    <!-- Oříznutí přílíš dlouhého textu -->

                                    <c:if test="${!order.standingOrder.message.equals(\"\")}">
                                        <c:choose>
                                            <c:when test="${fn:length(order.standingOrder.message) > 36}">
                                                <c:set var="message">${fn:substring(order.standingOrder.message, 0, 36)}...</c:set>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="message">${order.standingOrder.message}</c:set>
                                            </c:otherwise>
                                        </c:choose>


                                        <div class="text-small"><span
                                                class="font-italic"
                                                title="${order.standingOrder.message}">zpráva: ${message}</span><span></span>
                                        </div>
                                    </c:if>

                                    <div class="text-small"><span class="font-italic">částka: </span><span
                                            class="text-bold"><fmt:formatNumber
                                            value="${order.standingOrder.amount}" type="currency"/></span>
                                    </div>

                                    <div class="text-small"><span class="font-italic">perioda: </span>
                                        <span>${order.standingOrder.period} min</span>
                                    </div>

                                    <div class="text-small"><span class="font-italic">aktivní: </span>
                                        <input type="checkbox" <c:if test="${order.standingOrder.active}">checked</c:if> disabled/>
                                    </div>

                                </div>

                                <div class="col-4 mt-auto mb-auto text-right">
                                    <c:choose>
                                        <c:when test="${order.standingOrder.active}">
                                            <a class="btn btn-sm btn-outline-secondary"
                                               href="${s:mvcUrl('StandingOrderCtl#switchState').arg(0, order.standingOrder.id).build()}">Deaktivovat</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a class="btn btn-sm btn-outline-success"
                                               href="${s:mvcUrl('StandingOrderCtl#switchState').arg(0, order.standingOrder.id).build()}">Aktivovat</a>
                                        </c:otherwise>
                                    </c:choose>

                                    <a class="btn btn-sm btn-outline-primary"
                                       href="${s:mvcUrl('StandingOrderCtl#editOrder').arg(0, order.standingOrder.id).build()}">Upravit</a>
                                    <a class="btn btn-sm btn-outline-danger"
                                       href="${s:mvcUrl('StandingOrderCtl#deleteOrder').arg(0, order.standingOrder.id).build()}">Smazat</a>
                                </div>
                            </div>

                            <hr>

                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>