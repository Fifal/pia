<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Editace šablony</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Editace šablony</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <a class="btn btn-sm btn-outline-success float-right" href="${s:mvcUrl('TemplatesCtl#list').build()}"><span class="fa fa-arrow-left fa-fw"></span> Zpět k výpisu šablon</a>
                            </div>
                        </div>
                        <hr>

                        <c:choose>
                            <c:when test="${template.id != 0}">
                                <c:set var="action">
                                    ${s:mvcUrl('TemplatesCtl#editTemplate').arg(0, template.id).build()}
                                </c:set>
                            </c:when>

                            <c:otherwise>
                                <c:set var="action">
                                    ${s:mvcUrl('TemplatesCtl#addTemplateSubmit').build()}
                                </c:set>
                            </c:otherwise>
                        </c:choose>


                        <form:form modelAttribute="template" method="post" action="${action}" class="form">
                            <!-- Název šablony -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="title" for="title"
                                                cssClass="text-small font-weight-bold">Název šablony *</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="title" type="text" class="form-control"/>
                                </div>
                            </div>

                            <!-- Chyba pro název -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="title"/></span>
                                </div>
                            </div>

                            <!-- Číslo účtu -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="benefAccount" for="benefAccount"
                                                cssClass="text-small font-weight-bold">Číslo účtu *</form:label>
                                </div>
                                <div class="col-md-5">
                                    <form:input path="benefAccount" class="form-control" id="benefAccount" type="text"
                                                placeholder="Zadejte číslo účtu."/>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text" id="banka-addon">
                                            <span class="fa fa-university fa-fw"></span>
                                        </span>
                                        </div>
                                        <form:select path="benefBankCode" id="benefBankCode" class="form-control"
                                                     aria-describedby="banka-addon">
                                            <c:forEach items="${bankCodeList}" var="bc">
                                                <form:option value="${bc.code}">${bc.code} – ${bc.name}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                </div>
                            </div>

                            <!-- Chyby pro číslo účtu -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="benefAccount"/></span>
                                    <span class="text-small text-danger"><form:errors path="benefBankCode"/></span>
                                </div>
                            </div>

                            <!-- Částka -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="amount" for="amount"
                                                cssClass="text-small font-weight-bold">Částka *</form:label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <form:input path="amount" class="form-control text-right" id="amount"
                                                    type="text" aria-describedby="mena-addon"/>
                                        <div class="input-group-append">
                                            <select class="form-control" id="mena-addon">
                                                <option>CZK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Chyba pro částku -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="amount"/></span>
                                </div>
                            </div>

                            <!-- Variabilní symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="varSymbol" for="varSymbol"
                                                cssClass="text-small">Variabilní symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="varSymbol" type="text" class="form-control"/>
                                </div>
                            </div>

                            <!-- Chyba pro variabilní symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="varSymbol"/></span>
                                </div>
                            </div>

                            <!-- Konstantní symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="constSymbol" for="constSymbol"
                                                cssClass="text-small">Konstantní symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="constSymbol" type="text" class="form-control"/>
                                </div>
                            </div>

                            <!-- Chyba pro konstantní symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="constSymbol"/></span>
                                </div>
                            </div>

                            <!-- Specifický symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="specSymbol" for="specSymbol"
                                                cssClass="text-small">Specifický symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="specSymbol" type="text" class="form-control"/>
                                </div>
                            </div>

                            <!-- Chyba pro specifický symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="specSymbol"/></span>
                                </div>
                            </div>

                            <!-- Zpráva pro příjemce -->
                            <div class="row mb-4">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="message" for="message"
                                                cssClass="text-small">Zpráva pro příjemce</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="message" type="text" class="form-control"/>
                                </div>
                            </div>

                            <!-- Chyba pro zprávu pro příjemce -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="message"/></span>
                                </div>
                            </div>

                            <!-- Odeslání -->
                            <div class="row">
                                <div class="col-md-11">
                                    <input class="btn btn-outline-primary btn float-right" id="submit" type="submit"
                                           value="Uložit šablonu">
                                </div>
                            </div>


                        </form:form>
                    </div>

                    <div class="card-footer">
                        <div class="text-small text-black-50 text-center">Políčka označená * jsou povinná.</div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>