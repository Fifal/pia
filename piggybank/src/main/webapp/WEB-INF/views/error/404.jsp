<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<t:layout-default>
    <jsp:attribute name="title">Něco se pokazilo</jsp:attribute>

    <jsp:body>
        <c:set var="cardBg" value="${isAdmin ? 'card-header-blue' : 'card-header-green'}"/>

        <div class="container container-main">
            <t:flash-messages/>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <h2 class="card-header ${cardBg}"></h2>
                        <div class="card-body">
                            <h1>Chyba #404</h1>
                            <hr>
                            <p class="text-small">Něco se pokazilo a požadovaná stránka nebyla nalezena.</p>
                            <t:error-backlink/>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-default>