package test;

import cz.filek.pia.model.*;
import cz.filek.pia.model.enums.ERole;
import cz.filek.pia.repository.*;
import cz.filek.pia.services.MailService;
import cz.filek.pia.services.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * Tests for UserService
 *
 * @author Filip Jani
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest
{
    private UserRepository userRepository;
    private AccountRepository accountRepository;
    private CardRepository cardRepository;
    private UserCardRepository userCardRepository;
    private BankCodeRepository bankCodeRepository;
    private UserAccountRepository userAccountRepository;
    private UserAddressRepository userAddressRepository;
    private UserRoleRepository userRoleRepository;
    private UserPaymentTemplatesRepository userPaymentTemplatesRepository;
    private RoleRepository roleRepository;
    private MailService mailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UserService userService;

    @Before
    public void setUp()
    {
        this.userRepository = mock(UserRepository.class);
        this.accountRepository = mock(AccountRepository.class);
        this.cardRepository = mock(CardRepository.class);
        this.userCardRepository = mock(UserCardRepository.class);
        this.bankCodeRepository = mock(BankCodeRepository.class);
        this.userAccountRepository = mock(UserAccountRepository.class);
        this.userAddressRepository = mock(UserAddressRepository.class);
        this.userRoleRepository = mock(UserRoleRepository.class);
        this.userPaymentTemplatesRepository = mock(UserPaymentTemplatesRepository.class);
        this.roleRepository = mock(RoleRepository.class);
        this.mailService = mock(MailService.class);
        this.bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);

        this.userService = new UserService(
                userRepository,
                accountRepository,
                cardRepository,
                userCardRepository,
                bankCodeRepository,
                userAccountRepository,
                userAddressRepository,
                userRoleRepository,
                userPaymentTemplatesRepository,
                roleRepository,
                mailService,
                bCryptPasswordEncoder
        );
    }

    /**
     * Tests that if repository returns List of Users
     */
    @Test
    public void getUsersList()
    {
        User user = new User();
        user.setId(1);

        User user2 = new User();
        user2.setId(2);

        ArrayList<User> users = new ArrayList<User>(){{add(user); add(user2);}};

        when(userRepository.findAllByRole(ERole.user, new Sort(Sort.Direction.ASC, "id"))).thenReturn(users);

        Assert.assertEquals(2, this.userService.getUsersList().size());
        Assert.assertEquals(1, this.userService.getUsersList().get(0).getId());
        Assert.assertEquals(2, this.userService.getUsersList().get(1).getId());
    }

    /**
     * Tests that userService will return null when repository returns null
     */
    @Test
    public void getUsersList_null()
    {
        ArrayList<User> users = new ArrayList<>();

        when(userRepository.findAllByRole(ERole.user, new Sort(Sort.Direction.ASC, "id"))).thenReturn(users);

        Assert.assertEquals(0, this.userService.getUsersList().size());
    }

    /**
     * Test generation of user card
     */
    @Test
    public void generateUserCard()
    {
        User user = new User();
        user.setId(1);

        when(userRepository.findById(user.getId())).thenReturn(user);

        UserCard userCard = userService.generateUserCard(user.getId(), 1);

        Assert.assertNotNull(userCard.getCard());
        Assert.assertEquals(user, userCard.getUser());

        // verifies that card was saved and assigned to user
        verify(cardRepository, times(1)).save(any());
        verify(userCardRepository, times(1)).save(any());
    }

    /**
     * Test generation of user account
     */
    @Test
    public void generateUserAccount()
    {
        User user = new User();

        UserAccount userAccount = userService.generateUserAccount(user);

        Assert.assertNotNull(userAccount.getAccount());
        Assert.assertNotNull(userAccount.getAccount().getNumber());
        Assert.assertNotNull(userAccount.getUser());

        // verifies that account was saved and assigned to user
        verify(accountRepository, times(1)).save(any());
        verify(userAccountRepository, times(1)).save(any());
    }

    /**
     * Test of adding new User, that login and password was generated
     */
    @Test
    public void addNewUser()
    {
        Role role = new Role();
        role.setRole(ERole.user);
        User user = new User();
        user.setEmail("test@test.cz");
        UserAddress userAddress = new UserAddress();
        userAddress.setUser(user);

        when(roleRepository.findByRole(ERole.user)).thenReturn(role);
        when(userRepository.save(any())).thenAnswer((Answer<User>) invocationOnMock -> (User)invocationOnMock.getArguments()[0]);
        when(bCryptPasswordEncoder.encode(any())).thenReturn("1234");

        User savedUser = userService.addNewUser(userAddress);

        // User should have generated new login and password
        Assert.assertNotNull(savedUser.getLogin());
        Assert.assertNotNull(savedUser.getPassword());

        // verifies that user was saved
        verify(userRepository, times(1)).save(user);
        // verifies that user address was saved
        verify(userAddressRepository, times(1)).save(userAddress);
        // verifies that user role was created and saved
        verify(userRoleRepository, times(1)).save(any());
        // verifies that email about account creation was send to client
        verify(mailService, times(1)).sendMailAccountCreated(eq(user.getEmail()), any(), any());
        // verifies that new account was created
        verify(accountRepository, times(1)).save(any());
        // verifies that account was assigned to user
        verify(userAccountRepository, times(1)).save(any());
    }

    /**
     * Tests updating of user
     */
    @Test
    public void updateUser()
    {
        User user = new User();
        user.setId(1);
        UserAddress userAddress = new UserAddress();
        userAddress.setUser(user);

        when(userRepository.findById(1)).thenReturn(user);

        Assert.assertEquals(userAddress, userService.updateUser(userAddress));

        verify(userRepository, times(1)).save(user);
        verify(userAddressRepository, times(1)).save(userAddress);
    }

    /**
     * Tests updating of user address
     */
    @Test
    public void updateUserAddress()
    {
        User user = new User();
        user.setId(1);
        UserAddress userAddress = new UserAddress();
        userAddress.setId(1);
        userAddress.setUser(user);

        when(userAddressRepository.findUserAddressByUserId(1)).thenReturn(userAddress);

        Assert.assertEquals(userAddress, userService.updateUserAddress(user, userAddress));

        verify(userRepository, times(1)).save(user);
        verify(userAddressRepository, times(1)).save(userAddress);
    }

    /**
     * Tests that service calls userRepository.delete() when user was found
     * and that mailService sends an email to the client
     */
    @Test
    public void deleteUser_shouldPass()
    {
        User user = new User();

        when(userRepository.findById(1)).thenReturn(user);

        Assert.assertTrue(userService.deleteUser(1));

        verify(userRepository, times(1)).delete(user);
        verify(mailService, times(1)).sendMailAccountDeleted(any(), any());
    }

    /**
     * Tests that service returns false when deletion of user was not successful
     */
    @Test
    public void deleteUser_shouldFail()
    {
        when(userRepository.findById(1)).thenReturn(null);

        Assert.assertFalse(userService.deleteUser(1));

        verify(userRepository, never()).delete(any());
        verify(mailService, never()).sendMailAccountDeleted(any(), any());
    }

    /**
     * Tests that service returns value from repository
     */
    @Test
    public void getUser()
    {
        User user = new User();

        when(userRepository.findById(1)).thenReturn(user);

        Assert.assertEquals(user, userService.getUser(1));
    }

    /**
     * Tests that service returns value from repository
     */
    @Test
    public void getUserAddress()
    {
        UserAddress userAddress = new UserAddress();

        when(userAddressRepository.findUserAddressByUserId(1)).thenReturn(userAddress);

        Assert.assertEquals(userAddress, userService.getUserAddress(1));
    }

    /**
     * Tests that service returns correct values in list
     */
    @Test
    public void getUserAccounts()
    {
        UserAccount userAccount = new UserAccount();
        UserAccount userAccount1 = new UserAccount();

        ArrayList<UserAccount> list = new ArrayList<UserAccount>(){{ add(userAccount); add(userAccount1); }};

        when(userAccountRepository.findUserAccountsByUserId(1)).thenReturn(list);

        Assert.assertEquals(2, userService.getUserAccounts(1).size());
        Assert.assertEquals(userAccount, userService.getUserAccounts(1).get(0));
    }

    /**
     * Tests that service should return null when repository returns null
     */
    @Test
    public void getUserAccounts_null(){
        when(userAccountRepository.findUserAccountsByUserId(1)).thenReturn(null);

        Assert.assertNull(userService.getUserAccounts(1));
    }

    /**
     * Test that service returns correct list
     */
    @Test
    public void getUserCards()
    {
        UserCard userCard = new UserCard();
        UserCard userCard2 = new UserCard();

        ArrayList<UserCard> list = new ArrayList<UserCard>(){{ add(userCard); add(userCard2);}};

        when(userCardRepository.findUserCardsByUserId(1)).thenReturn(list);

        Assert.assertEquals(2, userService.getUserCards(1).size());
        Assert.assertEquals(userCard, userService.getUserCards(1).get(0));
    }

    /**
     * Tests that if card is found that result of the service should be true
     * and cardRepository.delete() should be called once
     */
    @Test
    public void deleteCard_shouldPass()
    {
        Card card = new Card();
        card.setId(1);

        UserCard userCard = new UserCard();
        userCard.setCard(card);

        when(userCardRepository.findUserCardByCardId(card.getId())).thenReturn(userCard);

        Assert.assertTrue(userService.deleteCard(card.getId()));

        verify(cardRepository, times(1)).delete(card);
    }

    /**
     * Tests that if card is not found that result of the service method should be false
     * and cardRepository.delete() shouldn't be called at all
     */
    @Test
    public void deleteCard_shouldFail()
    {
        when(userCardRepository.findUserCardByCardId(1)).thenReturn(null);

        Assert.assertFalse(userService.deleteCard(1));

        verify(cardRepository, never()).delete(any());
    }

    /**
     * Test that service returns correct user account
     */
    @Test
    public void getUserAccount()
    {
        UserAccount userAccount = new UserAccount();
        when(userAccountRepository.findUserAccountByAccountId(1)).thenReturn(userAccount);

        Assert.assertEquals(userAccount, userService.getUserAccount(1));
    }

    /**
     * Tests that service returns correct user by card ID
     */
    @Test
    public void getUserByCardId()
    {
        User user = new User();
        UserCard userCard = new UserCard();
        userCard.setUser(user);

        when(userCardRepository.findUserCardByCardId(1)).thenReturn(userCard);

        Assert.assertEquals(user, userService.getUserByCardId(1));
    }

    /**
     * Tests if service returns null if user card is not found
     */
    @Test
    public void getUserByCardId_null()
    {
        when(userCardRepository.findUserCardByCardId(1)).thenReturn(null);

        Assert.assertNull(userService.getUserByCardId(1));
    }

    /**
     * Tests that service returns correct user by login
     */
    @Test
    public void getUserByLogin()
    {
        User user = new User();
        when(userRepository.findUserByLogin("login")).thenReturn(user);

        Assert.assertEquals(user, userService.getUserByLogin("login"));
    }

    /**
     * Tests that service returns correct list of roles to user
     */
    @Test
    public void getUserRole()
    {
        User user = new User();
        user.setId(1);

        User notExisting = new User();
        notExisting.setId(2);

        ArrayList<UserRole> list = new ArrayList<UserRole>(){{add(new UserRole());}};

        when(userRoleRepository.findUserRolesByUserId(user.getId())).thenReturn(list);

        Assert.assertEquals(1, userService.getUserRole(user).size());
        Assert.assertEquals(0, userService.getUserRole(notExisting).size());
    }

    /**
     * Tests that service returns correct templates to users
     */
    @Test
    public void getUserPaymentTemplates()
    {
        User user = new User();
        user.setId(1);

        User notExisting = new User();
        notExisting.setId(2);


        ArrayList<UserPaymentTemplate> list = new ArrayList<UserPaymentTemplate>(){{
            add(new UserPaymentTemplate());
            add(new UserPaymentTemplate());
            add(new UserPaymentTemplate());
        }};

        when(userPaymentTemplatesRepository.findByUser(user)).thenReturn(list);

        Assert.assertEquals(3, userService.getUserPaymentTemplates(user).size());
        Assert.assertEquals(0, userService.getUserPaymentTemplates(notExisting).size());
    }

    /**
     * Tests if repository returns null payment templates for user, service returns null as well
     */
    @Test
    public void getUserPaymentTemplates_null()
    {
        User user = new User();
        when(userPaymentTemplatesRepository.findByUser(user)).thenReturn(null);

        Assert.assertNull(userService.getUserPaymentTemplates(user));
    }

    /**
     * Tests if repository returns user count, then service returns correct count
     */
    @Test
    public void getUsersCount()
    {
        when(userRepository.countAllByRole(ERole.user)).thenReturn(5);

        Assert.assertEquals(new Integer(5), userService.getUsersCount());
    }

    /**
     * Tests if repository returns null count then user service returns null
     */
    @Test
    public void getUsersCount_null()
    {
        when(userRepository.countAllByRole(ERole.user)).thenReturn(null);

        Assert.assertNull(userService.getUsersCount());
    }
}